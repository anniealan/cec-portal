import express from 'express';
import mongoose from "mongoose";


const MONGO_HOST = process.env.MONGO_HOST || 'localhost';

mongoose.connect(`mongodb://${MONGO_HOST}:27017/cec_portal`, {
  useUnifiedTopology: true,
  useNewUrlParser: true
});

const db = mongoose.connection;

db.once('open', () => {  // Check connection
  console.log('Connected to MongoDB');
});

db.on('error', (err) => {  // Check for DB errors
  console.log(err);
});


// Initialize
const app = express();

const HOST = process.env.HOST || 'localhost';

app.listen(5000, HOST, () => {
  console.log('Server is listening on PORT 5000');
});


app.get("*",  (req, res) => {
	res.send("API is working");
});


